<?php

/**
 * @file
 * Advanced Batch administration pages.
 */

/**
 * Task manager.
 */
function advbatch_admin_task_manager() {
  $headers = array('', t("Batch"), t("Progress"), t("Queued"), t("Started"), t("Duration"), t("ETA"), t("Backend"), t("Operations"));
  $rows    = array();
  $colspan = count($headers);

  try {
    foreach (AdvancedBatch_Controller::getInstance()->getTaskQueue() as $task) {
      // This test is only here for IDE automatic completion.
      if ($task instanceof AdvancedBatch_Task) {
        $row      = array();

        $batch    = $task->getBatch();
        $created  = $task->getCreationTime();
        $started  = $task->getStartTime();
        $finished = $task->getFinishedTime();
        $duration = $task->getDuration();
        $progress = $task->getProgress();

        if ($backend = $task->getBackend()) {
          $backendName = $backend->getName();
        }
        else {
          $backendName = '<em>' . t("None") . '</em>';
        }

        if (($started && $duration && !$finished)) {
          $eta = AdvancedBatch_Helper::formatDuration(AdvancedBatch_Helper::estimateEta($duration, $progress), AdvancedBatch_Helper::FORMAT_TIME_MORE_THAN);
        }
        else {
          $eta = t("N/A");
        }

        try {
          $row[] = $task->getTaskId();
          $row[] = $batch->getDescription();
          $row[] = theme('task_progress', array('task' => $task));
          $row[] = AdvancedBatch_Helper::formatDate($created, TRUE);
          $row[] = $started  ? AdvancedBatch_Helper::formatDate($started)  : t("Waiting");
          $row[] = AdvancedBatch_Helper::formatDuration($duration, AdvancedBatch_Helper::FORMAT_TIME_FULL);
          $row[] = $eta;
          $row[] = $backendName;
          $row[] = ''; // FIXME: Implement this.
        }
        catch (AdvancedBatch_Exception $e) {
          $row = array();
          $row[] = $task->getTaskId();
          $row[] = array('colspan' => $colspan - 1, 'data' => t("Technical error while querying for this task."));
          watchdog_exception('advbatch', $e);
        }

        $rows[] = $row;
      }
    }
  }
  catch (AdvancedBatch_Exception $e) {
    drupal_set_message(t("An error happened, contact site administrator."), 'error');
    watchdog_exception('advbatch', $e);
  }

  return theme('table', array(
    'header' => $headers,
    'rows' => $rows,
  )) . theme('pager', array());
}

/**
 * Backends status page.
 */
function advbatch_admin_backend_status() {
  // FIXME TODO.
  return "BACKEND STATUS";
}

/**
 * Backends configuration page.
 */
function advbatch_admin_backend_settings() {
  // FIXME TODO.
  return "BACKEND CONFIGURATION";
}

/**
 * List of all known tasks.
 */
function advbatch_admin_system() {
  $headers = array(t("Module"), t("Task"), t("Description"), t("Operations"));
  $rows    = array();

  foreach (module_implements('batches') as $module) {
    foreach (module_invoke($module, 'batches') as $name => $item) {
      $row = array();

      $row[] = $module;
      $row[] = $item['name'];
      $row[] = isset($item['description']) ? $item['description'] : NULL;
      $row[] = theme('links', array('links' => array(
        array(
          'title' => t("enqueue"),
          'href' => 'admin/tasking/manager/queue-task/' . $name,
        ),
      )));

      $rows[] = $row;
    }
  }

  return theme('table', array(
    'header' => $headers,
    'rows' => $rows,
  ));
}

/**
 * Enqueue new task form.
 * 
 * This form be embed pretty much where you want.
 */
function advbatch_admin_enqueue($form, &$form_state, $task_name = NULL) {

  if (!isset($task_name)) {
    $options = array();
    foreach (module_invoke_all('batches') as $task_name => $info) {
      $options[$task_name] = isset($info['name']) ? $info['name'] : $task_name;
    }
    $form['task_name'] = array(
      '#type' => 'radios',
      '#title' => t("Select task"),
      '#options' => $options,
      '#required' => TRUE,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t("Select and enqueue task"),
    );
  }
  else {
    $form['task_name'] = array(
      '#type' => 'value',
      '#value' => $task_name,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t("Enqueue task"),
    );
  }

  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t("Cancel"), isset($_GET['destination']) ? $_GET['destination'] : 'admin/tasking/manager'),
  );

  return $form;
}

/**
 * Submit handler for enqueue form.
 */
function advbatch_admin_enqueue_submit($form, &$form_state) {

  // FIXME: Ugly somehow, but will work.
  if (!isset($form_state['values']['task_name']) || !($task = advbatch_menu_task_load($form_state['values']['task_name']))) {
    drupal_set_message(t("Invalid task couldn't be enqueued"), 'error');
    return;
  }

  try {
    AdvancedBatch_Controller::getInstance()->queueTask($task);
    drupal_set_message(t("New task enqueued with identifier <em>@identifier</em>.", array('@identifier' => $task->getTaskId())));

    if (!isset($_GET['destination'])) {
      $form_state['redirect'] = 'admin/tasking/manager';
    }
  }
  catch (AdvancedBatch_Exception $e) {
    drupal_set_message(t("Invalid task couldn't be enqueued"), 'error');
    watchdog_exception('advbatch', $e);
  }
}

/**
 * Display batch progress.
 */
function template_preprocess_task_progress(&$variables) {
  if (($task = $variables['task']) instanceof AdvancedBatch_Task) {
    $progress                     = $task->getProgress();
    $variables['progress_string'] = AdvancedBatch_Helper::formatProgress($progress);
    $variables['progress']        = $progress;
    $variables['status']          = AdvancedBatch_Task::statusToString($task->getStatus());
  }
  else {
    $variables['progress_string'] = '';
    $variables['progress']        = 0;
    $variables['status']          = t("Unknown");
  }
}
