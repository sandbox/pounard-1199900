<?php

/**
 * @file
 * Procedural batch sample.
 */

/**
 * Sample batch definition.
 * 
 * Pretty much as the real batch API, this function returns a valid core batch.
 * This batch will be processed as hard as possible, it basically means that
 * some just won't work, because they will mess up with their own limit and
 * total.
 * 
 * But hopping they use $context as in the examples they saw, we will be able
 * to fetch some data inside and be able to process it.
 */
function advbatch_sample_batch($operations_count, $iterations_count) {
  $operations = array();

  // Build a fake array of operations for testing purposes.
  for ($i = 0; $i < $operations_count; ++$i) {
    $operations[] = array('_advbach_sample_batch_operation', array($iterations_count));
  }

  return array(
    'operations' => $operations,
    'finished' => '_some_non_existing_function_that_will_be_ignored',
    'title' => t('Processing'),
    'progress_message' => '',
    'error_message' => t('The update has encountered an error.'),
  );
}

/**
 * Sample batch operation.
 * 
 * Pretty much as the real batch API, we will pass here a fake context.
 */
function _advbach_sample_batch_operation(&$context) {
  sleep(1);
}
