<?php

/**
 * @file
 * Advanced Batch hook definition.
 */

/**
 * Define system known batches. Use this hook in order to define your batches
 * if you want the site administrator to be able to run them manually out of
 * any context.
 * 
 * Any configuration dependent batch should not be visible in this hook, else
 * the user will be able to run them anyway, which can cause unpredictable
 * behaviors depending on the custom batch implementation.
 * 
 * This hook will be called in administration pages only. Its result will not
 * be cached, you can safely use the t() function here.
 */
function hook_batches() {
  $items = array();

  $items['sample_object'] = array(
    'class'       => 'AdvancedBatch_Sample_Batch',
    'name'        => t("Sample object oriented batch"),
    'description' => t("Fully featured sample batch of object oriented code."),
    'options'     => array(
      'iterations' => 30,
      'operations' => 10,
    ),
  );

  $items['sample_procedural'] = array(
    'class'       => 'AdvancedBatch_Batch_CoreBatch',
    'name'        => t("Sample procedural batch"),
    'description' => t("Fully featured sample batch of procedural code."),
    'options'     => array(
      'function'  => 'advbatch_sample_batch',
      'file'      => 'advbatch_sample.procedural.inc',
      'path'      => drupal_get_path('module', 'advbatch_sample'),
      'args'      => array(10, 30),
    ),
  );

  return $items;
}

/**
 * This hook will be called in both administration pages and hook_cron().
 * 
 * This hook result will not be cached, you can safely use the t() function
 * here.
 * 
 * It defines the batches that must be run on a regular basis. You could skip
 * this hook and manually queue your batches in cron, but using this hook will
 * allow advanced administration screens to be created and managed through the
 * web by the site administrator.
 * 
 * The signature is the same as the hook_batches(), except you should add an
 * additional 'delay' key with delay in second the task should wait before
 * running again.
 */
function hook_batches_recurring() {
  $items = array();

  $items['sample_recurring'] = array(
    'class'       => 'AdvBatch_Sample_Batch',
    'name'        => t("Sample object oriented batch"),
    'description' => t("Fully featured sample batch of object oriented code."),
    'delay'       => 3600,
  );

  return $items;
}
