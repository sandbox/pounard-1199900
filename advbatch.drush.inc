<?php

/**
 * Implements of hook_drush_command().
 */
function advbatch_drush_command() {
  $items = array();
  $items['advbatch-system-list'] = array(
    'description' => dt('Print known batch list'),
    'aliases' => array('abl'),
  );
  $items['advbatch-queue'] = array(
    'description' => dt('Print queued task list'),
    'aliases' => array('abq'),
  );
  $items['advbatch-run'] = array(
    'description' => dt('Run specific queued task in CLI'),
    'aliases' => array('abr'),
    'arguments' => array(
      'taskId' => 'Task identifier',
    ),
  );
  $items['advbatch-task'] = array(
    'description' => dt('Change a task state manually'),
    'aliases' => array('abt'),
    'arguments' => array(
      'taskId' => 'Task identifier',
      'command' => "Operation to perform on selected task. Can be one of 'retry' or 'pause', 'stop'",
    ),
  );
  return $items;
}

function drush_advbatch_task($taskId = NULL, $command = NULL) {
  // Display task information.
  if (!isset($taskId)) {
    drush_set_error(dt("You must specify a task identifier."));
    return -1;
  }

  try {
    $task = AdvancedBatch_Controller::getInstance()->getTask($taskId);

    $batch          = $task->getBatch();
    $operationCount = count($batch->getOperations());
    $status         = $task->getStatus();

    // Display some information.
    drush_print(dt('Selected batch is @taskId - @description', array(
      '@taskId' => $taskId,
      '@description' => $batch->getDescription(),
    )));

    drush_print(dt('Task status is: @status', array('@status' => AdvancedBatch_Task::statusToString($status))));

    if (!isset($command)) {
      return 0;
    }

    switch ($command) {

      case 'retry':
        if (in_array($status, array(
          AdvancedBatch_Task::STATE_CANCELED,
          AdvancedBatch_Task::STATE_ERROR,
          AdvancedBatch_Task::STATE_RUNNING,
        )))
        {
          AdvancedBatch_Controller::getInstance()->updateTask($task, array('status' => AdvancedBatch_Task::STATE_STALLED));
          drush_log(dt('Task reverted to status @status.', array('@status' => AdvancedBatch_Task::statusToString(AdvancedBatch_Task::STATE_STALLED))), 'success');
        }
        else {
          drush_log(dt('Nothing to do.'), 'success');
        }
        return 0;

      case 'pause':
        // FIXME: Implement this.
        drush_set_error(dt("Not implemented yet."));
        return -1;

      case 'stop':
        // FIXME: Implement this.
        drush_set_error(dt("Not implemented yet."));
        return -1;

      default:
        drush_set_error(dt("Unknown command '@command'.", array('@command' => $command)));
        return -1;
    }
  }
  catch (AdvancedBatch_Exception $e) {
    drush_set_error(dt('Exception happened:') . ' ' . $e->getMessage());
  }
}

function drush_advbatch_system_list() {
  $headers = array(dt('Id.'), dt('Desc.'), dt('Status'), '', dt('Queued'), dt('Started'), dt('Backend'));
  $rows    = array();
  $rows[]  = $headers;
  // FIXME: Todo.
  drush_print_table($rows, TRUE);
}

function drush_advbatch_queue() {
  $headers = array(dt('Id.'), dt('Desc.'), dt('Status'), '', dt('Queued'), dt('Started'), dt('ETA'), dt('Backend'));
  $rows    = array();
  $rows[]  = $headers;
  foreach (AdvancedBatch_Controller::getInstance()->getTaskQueue() as $task) {
    // This test is only here for IDE automatic completion.
    if ($task instanceof AdvancedBatch_Task) {
      $row      = array();
      $batch    = $task->getBatch();
      $created  = $task->getCreationTime();
      $started  = $task->getStartTime();
      $finished = $task->getFinishedTime();
      $duration = $task->getDuration();
      $progress = $task->getProgress();
      $eta      = (!$finished && $duration && $progress) ? AdvancedBatch_Helper::estimateEta($duration, $progress) : NULL;
      try {
        $row[] = sprintf("% 4d", $task->getTaskId());
        $row[] = $batch->getDescription();
        $row[] = AdvancedBatch_Task::statusToString($task->getStatus());
        $row[] = AdvancedBatch_Helper::formatProgress($progress);
        $row[] = AdvancedBatch_Helper::formatDate($created);
        $row[] = $started  ? AdvancedBatch_Helper::formatDate($started) : t("Queued");
        $row[] = $eta ? AdvancedBatch_Helper::formatDuration($eta) : dt("N/A");
        $row[] = '';
      }
      catch (AdvancedBatch_Exception $e) {
        $row = array();
        $row[] = $task->getTaskId();
        $row[] = array('colspan' => $colspan - 1, 'data' => t("Technical error while querying for this task."));
        watchdog_exception('advbatch', $e);
      }
      $rows[] = $row;
    }
  }
  drush_print_table($rows, TRUE);
}

function drush_advbatch_run($taskId = NULL, $timeLimit = NULL) {
  if (!$taskId) {
    drush_set_error(dt("You must specify a task identifier."));
    return -1;
  }

  // We probably cant use this constant in method signature, because this
  // function code will be parsed when Drupal is not bootstrapped and
  // class doesn't exists, which would cause PHP to WSOD in CLI.
  $timeLimit = (int) $timeLimit;
  if (!isset($timeLimit)) {
    $timeLimit = AdvancedBatch_Runner::TIME_LIMIT_NONE;
  }

  try {
    $task = AdvancedBatch_Controller::getInstance()->getTask($taskId);

    $batch          = $task->getBatch();
    $operationCount = count($batch->getOperations());

    // Display some information.
    drush_print(dt('Selected batch is @taskId - @description', array(
      '@taskId' => $taskId,
      '@description' => $batch->getDescription(),
    )));

    switch ($task->getStatus()) {
      case AdvancedBatch_Task::STATE_QUEUED:
        drush_print(dt("This batch has @operationCount operations.", array('@operationCount' => $operationCount)));
        break;

      case AdvancedBatch_Task::STATE_ERROR:
        drush_log(dt('This batch encountered errors. You can still try to resume it but most implementations will fail trying.'), 'warning');
        break;

      case AdvancedBatch_Task::STATE_RUNNING:
        drush_log(dt('This batch is already running. If its database state is erroenous and you want to resume it, please force a cleanup using the advbatch-task command.'), 'warning');
        return -1;

      case AdvancedBatch_Task::STATE_FINISHED:
        drush_print(dt("This batch is finished."));
        return 0;

      default:
        drush_print(dt("This batch will be resumed at operation @operationIndex/@operationCount", array(
          '@operationIndex' => $task->getOperationIndex() + 1,
          '@operationCount' => $operationCount,
        )));
        drush_print(dt("Estimated running ETA is @eta", array(
          '@eta' => AdvancedBatch_Helper::formatDuration(
                               AdvancedBatch_Helper::estimateEta($task->getDuration(), $task->getProgress()),
                               AdvancedBatch_Helper::FORMAT_TIME_FULL)
        )));
        break;
    }

    // Ask user for confirmation.
    if (!drush_confirm(dt('Run the selected task?'))) {
      return 0;
    }

    // In Drush, we can afford to run the task with unlimited time limit.
    $runner = $task->getRunner(0, $timeLimit);
    $runner->addListener(new AdvancedBatch_Drush_Listener($task));
    $runner->run();
  }
  catch (AdvancedBatch_Exception $e) {
    drush_set_error(dt('Exception happened:') . ' ' . $e->getMessage());
  }
}
