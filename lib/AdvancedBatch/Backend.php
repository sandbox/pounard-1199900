<?php

/**
 * Backend controller.
 */
class AdvancedBatch_Backend {
  /**
   * @var array
   */
  protected static $_instances = array();

  /**
   * Get backend.
   * 
   * @param string $backendName = NULL
   *   If non set, fetch the default one, or the one on top of the list.
   * 
   * @return AdvancedBatch_Backend_Interface
   */
  public static function getBackend($backendName = NULL) {
    if (!isset(self::$_instances[$backendName])) {
      // FIXME: This may worth a static cache.
      $info = module_invoke_all('batch_backend');
      if (!isset($info[$backendName])) {
        throw new AdvancedBatch_Exception("Backend '" . $backendName . "' does not exists.");
      }
      if (!class_exists($info[$backendName]['class'])) {
        throw new AdvancedBatch_Exception("Class '" . $info[$backendName]['class'] . "' does not exists.");
      }
      self::$_instances[$backendName] = new $info[$backendName]['class'];
    }
    return self::$_instances[$backendName];
  }
}
