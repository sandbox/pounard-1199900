<?php

/**
 * Default controller listener. Updates task status in database while listening
 * to the currently runner.
 * 
 * Some users may want to implement their own. This default implementation can
 * be used as base, it will carefully take care of database updates.
 * 
 * FIXME: Manual update queries will be really more performant here.
 */
class AdvancedBatch_Task_Listener implements Event_Listener_Interface {
  /**
   * @var AdvancedBatch_Task
   */
  protected $_task;

  /**
   * @var int
   */
  protected $_timeStarted;

  /**
   * @var int
   */
  protected $_timeStopped;

  /**
   * @var int
   */
  protected $_timeDelta = 0;

  /**
   * Start time counter.
   */
  protected function _timeStart() {
    $this->_timeStarted = time();
  }

  /**
   * Stop time counter and update some variables.
   */
  protected function _timeUpdate() {
    $this->_timeStopped = time();
    $this->_timeDelta   = $this->_timeStopped - $this->_timeStarted;
  }

  /**
   * Get row ready for updating the existing task line entry.
   * 
   * @param AdvancedBatch_Runner $runner
   * 
   * @return stdClass
   */
  protected function _getRow(AdvancedBatch_Runner $runner) {
    $this->_timeUpdate();
    $row = new stdClass;
    $row->task_id   = $this->_task->getTaskId();
    $row->operation = $runner->getOperation();
    $row->offset    = $runner->getOffset();
    $row->limit     = $runner->getLimit();
    $row->duration  = $this->_task->getDuration() + $this->_timeDelta;
    $row->backend   = $this->getBackend();
    return $row;
  }

  /**
   * @var string
   */
  protected $_backend;

  /**
   * Allow subclasses to declare the backend they are actually using.
   */
  public function getBackend() {
    return $this->_backend;
  }

  /**
   * Batch error.
   * 
   * @param AdvancedBatch_Runner $runner
   */
  protected function _runing(AdvancedBatch_Runner $runner) {
    $this->_timeStart();
    $row = $this->_getRow($runner);
    $row->status = AdvancedBatch_Task::STATE_RUNNING;
    if (!isset($row->started)) {
      $row->started = $this->_timeStarted;
    }
    drupal_write_record('advbatch_queue', $row, array('task_id'));
  }

  /**
   * Batch error.
   * 
   * @param AdvancedBatch_Runner $runner
   */
  protected function _batchError(AdvancedBatch_Runner $runner) {
    $row = $this->_getRow($runner);
    $row->status   = AdvancedBatch_Task::STATE_ERROR;
    $row->finished = $this->_timeStopped;
    drupal_write_record('advbatch_queue', $row, array('task_id'));
  }

  /**
   * Batch error.
   * 
   * @param AdvancedBatch_Runner $runner
   */
  protected function _batchFinished(AdvancedBatch_Runner $runner) {
    $row = $this->_getRow($runner);
    $row->status = AdvancedBatch_Task::STATE_FINISHED;
    $row->finished = $this->_timeStopped;
    drupal_write_record('advbatch_queue', $row, array('task_id'));
  }

  /**
   * Batch error.
   * 
   * @param AdvancedBatch_Runner $runner
   */
  protected function _iterationDone(AdvancedBatch_Runner $runner) {
    $row = $this->_getRow($runner);
    $row->status = AdvancedBatch_Task::STATE_RUNNING;
    drupal_write_record('advbatch_queue', $row, array('task_id'));
  }

  /**
   * Batch error.
   * 
   * @param AdvancedBatch_Runner $runner
   */
  protected function _operationChange(AdvancedBatch_Runner $runner) {
    $row = $this->_getRow($runner);
    $row->status = AdvancedBatch_Task::STATE_RUNNING;
    drupal_write_record('advbatch_queue', $row, array('task_id'));
  }

  /**
   * Batch reached time limit.
   * 
   * @param AdvancedBatch_Runner $runner
   */
  protected function _timeLimitReached(AdvancedBatch_Runner $runner) {
    $row = $this->_getRow($runner);
    $row->status = AdvancedBatch_Task::STATE_STALLED;
    drupal_write_record('advbatch_queue', $row, array('task_id'));
  }

  public function event(Event_Sender_Interface $sender, $message = NULL, $args = NULL) {
    if (!$sender instanceof AdvancedBatch_Runner || !isset($message)) {
      return;
    }

    switch ($message) {

      case AdvancedBatch_Runner::EVENT_RUNNING:
        $this->_runing($sender);
        break;

      case AdvancedBatch_Runner::EVENT_BATCH_ERROR:
        $this->_batchError($sender);
        break;

      case AdvancedBatch_Runner::EVENT_BATCH_FINISHED:
        $this->_batchFinished($sender);
        break;

      case AdvancedBatch_Runner::EVENT_ITERATION_DONE:
        $this->_iterationDone($sender);
        break;

      case AdvancedBatch_Runner::EVENT_OPERATION_CHANGE:
        $this->_operationChange($sender);
        break;

      case AdvancedBatch_Runner::EVENT_TIME_LIMIT_REACHED:
        $this->_timeLimitReached($sender);
        break;
    }
  }

  /**
   * Default constructor.
   */
  public function __construct(AdvancedBatch_Task $task) {
    $this->_task = $task;
  }
}
