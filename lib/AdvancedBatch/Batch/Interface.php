<?php

/**
 * Long run opteration interface.
 */
interface AdvancedBatch_Batch_Interface {
  /**
   * Get localized short description for admin UI.
   * 
   * @return string
   *   Can also return NULL.
   */
  public function getDescription();

  /**
   * Return current batch token, if any.
   * 
   * @return int
   */
  public function getToken();

  /**
   * Set current batch token.
   * 
   * @param int $token
   */
  public function setToken($token);

  /**
   * Is this batch finished?
   * 
   * @return bool
   */
  public function isFinished();

  /**
   * Get progress as a float value between 0 and 1.
   * 
   * This function is purely informal, It must never change the object state
   * in any way, it can be called at various points in time, including while
   * running.
   * 
   * @return float
   */
  public function getProgress();

  /**
   * Get this batch operation list.
   * 
   * @return array
   *   Key/value pairs, keys are internal operation names while values are
   *   instances of AdvanceBatch_Operation_Interface.
   */
  public function getOperations();

  /**
   * Get operation at.
   * 
   * @param int $index
   * 
   * @return AdvancedBatch_Operation_Interface
   */
  public function getOperationAt($index);

  /**
   * Get current object options. This will be called when the batch stalls and
   * just before current task will be updated in queue.
   * 
   * You can use these options to set environmental and object configuration,
   * but please do not store any limit or offet there, rely on the resume()
   * method instead for resuming purposes.
   * 
   * @return array
   * 
   * @see AdvancedBatch_Batch_Interface::setOptions()
   */
  public function getOptions();

  /**
   * Set arbitrary options. This will be called on object resume and creation
   * when running in partial batched mode, or when displaying this batch state
   * in administration UI.
   * 
   * @param array $options
   */
  public function setOptions(array $options);

  /**
   * This batch is being resumed from a stalled state. This function must tell
   * the current operations before the given index that they are finished.
   * 
   * This method exists because some implementation could benefit from it for
   * resuming faster. Any resume operation optimization (discarding operations
   * options for example) could be done here.
   * 
   * @param int $operationIndex
   *   Operation index at which this batch was running before stall.
   * @param int $offset
   *   Offset on which the current operation stopped before stall.
   */
  public function resume($operationIndex, $offset);
}
