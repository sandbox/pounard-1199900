<?php

/**
 * Base implementation of AdvancedBatch_Batch_Interface.
 */
abstract class AdvancedBatch_Batch_Abstract implements AdvancedBatch_Batch_Interface {
  /**
   * @var int
   */
  protected $_token;

  /**
   * Return current batch token, if any.
   * 
   * @return int
   */
  public function getToken() {
    return $this->_token;
  }

  /**
   * Set current batch token.
   * 
   * @param int $token
   */
  public function setToken($token) {
    $this->_token = $token;
  }

  /**
   * @var bool
   */
  protected $_finished = FALSE;

  public function isFinished() {
    if (!$this->_finished) {
      $this->_finished = TRUE;
      foreach ($this->getOperations() as $operation) {
        if (!$operation->isFinished()) {
          $this->_finished = FALSE;
          break;
        }
      }
    }
    return $this->_finished;
  }

  /**
   * If you think you can compute a better progress than this default and
   * unaccurate implementation, then you should definitely implement it
   * yourself.
   * 
   * @see AdvancedBatch_Batch_Interface::getProgress()
   */
  public function getProgress() {
    $total = $done = 0;

    foreach ($this->getOperations() as $operation) {
      $weight = $operation->getWeight();

      if ($operationTotal = $operation->getTotal()) {
        // Offset NULL or 0 will gives us a result of 0 here.
        $done += ($operation->getOffset() / $operationTotal) * $weight;
      }

      $total += $weight;
    }

    // 0 total would been either we have no operations, either all have a 0
    // weight, in both case we cannot compute anything.
    return $total > 0 ? $done / $total : 1;
  }

  /**
   * @var array
   */
  protected $_operations;

  /**
   * Lazy initialize internal _operations array.
   */
  protected abstract function _initOperations();

  public function getOperations() {
    if (!isset($this->_operations)) {
      $this->_initOperations();
    }
    return $this->_operations;
  }

  public function getOperationAt($index) {
    if (!isset($this->_operations)) {
      $this->_initOperations();
    }
    if (!isset($this->_operations[$index])) {
      throw new AdvancedBatch_Exception("Operation '" . $index . "' does not exists.");
    }
    return $this->_operations[$index];
  }

  /**
   * @var array
   */
  protected $_options = array();

  public function getOptions() {
    return $this->_options;
  }

  public function setOptions(array $options) {
    $this->_options = $options;
  }

  /**
   * This default implementation will force operations to be inited. This can
   * be heavy, depending on your operations implementation.
   * 
   * It will fit nicely for batches that have a few operations, and for which
   * operations do not require heavy initialization (avoid SQL queries or cache
   * fetch in operation init).
   * 
   * @see AdvancedBatch_Batch_Interface::resume()
   */
  public function resume($operationIndex, $offset) {
    $this->_initOperations();

    foreach ($this->getOperations() as $index => $operation) {
      if ($index < $operationIndex) {
        $operation->setOffset($operation->getTotal());
      }
      else if ($index == $operationIndex) {
        $operation->setOffset($offset);
        break;
      }
    }
  }
}
