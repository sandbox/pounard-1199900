<?php

/**
 * procedural batch API wrapper.
 */
class AdvancedBatch_Batch_CoreBatch extends AdvancedBatch_Batch_Abstract {
  /**
   * Core batch description.
   * 
   * @var array
   */
  protected $_coreBatch; 

  protected function _initOperations() {
    // Ensure file and path if set.
    if (isset($this->_options['file']) && isset($this->_options['path'])) {
      require_once $this->_options['path'] . '/' . $this->_options['file'];
    }

    // Check for batch builder function existence.
    $function = $this->_options['function'];
    if (!isset($function) || !is_string($function)) {
      throw new AdvancedBatch_Exception("Batch is not a valid core batch definition.");
    }
    if (!function_exists($function)) {
      throw new AdvancedBatch_Exception("Batch function '" . $function . "' does not exists.");
    }

    // Check for build arguments, if any.
    $args = $this->_options['args'];
    if (isset($args) && !is_array($args)) {
      throw new AdvancedBatch_Exception("Args are not an array.");
    }
    // Run the build and fetch the original batch.
    if (isset($args)) {
      $this->_coreBatch = call_user_func_array($function, $args);
    }
    else {
      $this->_coreBatch = $function();
    }
    if (!is_array($this->_coreBatch)) {
      throw new AdvancedBatch_Exception("Batch returned by function '" . $function . "' is not an array.");
    }

    // Ensure batch custom file, if any.
    if (isset($this->_coreBatch['file'])) {
      require_once $this->_coreBatch['file'];
    }

    // Create operations.
    $this->_operations = array();
    foreach ($this->_coreBatch['operations'] as $desc) {
      list($function, $args) = $desc;
      // Always ensure operation is valid, a missing operation means a batch
      // we cannot run safely.
      if (!function_exists($function)) {
        throw new AdvancedBatch_Exception("Batch operation function '" . $function . "' does not exists.");
      }
      $this->_operations[] = new AdvancedBatch_Operation_CoreBatch($function, $args);
    }
  }

  public function getDescription() {
    return t("Unknown core batch");
  }
}
