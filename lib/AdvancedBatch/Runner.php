<?php

/**
 * AdvancedBatch batch runner.
 * 
 * The runner is responsible of using the given batch and run it during a
 * a fixed amount of time.
 * 
 * A batch that reached the given limit without being finished will be then
 * in 'stalled' state, which will allows us to have a finer controler over
 * what is happening in the UI. A batch that stalls during a too long amount
 * of time will be marked as paused, and can then be manually resumed.
 * 
 * Once running a batch, updates and notify are made using a listener system,
 * each low lever runner must register its own listener before actually call
 * the run() method in order to be notified of status updates while running.
 * 
 * Low level runners (Drush, batch API and such) are heavily advised to use
 * a runner instance for running batches.
 * 
 * @see AdvancedBatch_Listener_Interface
 */
class AdvancedBatch_Runner extends Event_Sender {
  /**
   * Event run when changing operation.
   */
  const EVENT_OPERATION_CHANGE              =  1;

  /**
   * Event run when limit is reached and runner gracefully stopped.
   */
  const EVENT_TIME_LIMIT_REACHED            =  2;

  /**
   * An iteration has been done.
   */
  const EVENT_ITERATION_DONE                =  3;

  /**
   * An iteration has been done.
   */
  const EVENT_BATCH_FINISHED                =  4;

  /**
   * An iteration has been done.
   */
  const EVENT_BATCH_ERROR                   =  5;

  /**
   * An iteration has been done.
   */
  const EVENT_RUNNING                       =  6;

  /**
   * Default time limit, most PHP distributions will have this limit set to
   * 30 seconds per default in HTTPd environment. We lower this arbitrarly
   * to 25 seconds to let 5 seconds for bootstrap and closing operations.
   */
  const TIME_LIMIT_DEFAULT                  = 25;

  /**
   * No time limit.
   */
  const TIME_LIMIT_NONE                     =  0;

  /**
   * @var int
   */
  protected $_limit = 0;

  /**
   * @var int
   */
  protected $_offset = 0;

  /**
   * @var int
   */
  protected $_operation = 0;

  /**
   * @var AdvancedBatch_Batch_Interface
   */
  protected $_batch;

  /**
   * @var int
   */
  protected $_timeLimit;

  /**
   * @var int
   */
  protected $_timeStart;

  /**
   * Get limit.
   * 
   * @return int
   */
  public function getLimit() {
    return $this->_limit;
  }

  /**
   * Get offset.
   * 
   * @return int
   */
  public function getOffset() {
    return $this->_offset;
  }

  /**
   * Get operation number this runner currently is.
   * 
   * @return int
   */
  public function getOperation() {
    return $this->_operationIndex;
  }

  /**
   * Get batch.
   * 
   * @return AdvancedBatch_Batch_Interface
   */
  public function getBatch() {
    return $this->_batch;
  }

  /**
   * Start internal timer.
   */
  public function timerStart() {
    $this->_timeStart = time();
  }

  /**
   * Check if timer has reached its limit.
   * 
   * @return bool
   *   TRUE if time limit has been reached. 
   */
  public function timerElapsed() {
    if (!$this->_timeLimit) {
      return FALSE;
    }

    $current = time();
    return $current - $this->_timeStart > $this->_timeLimit;
  }

  /**
   * Get current batch progress.
   * 
   * @return float
   */
  public function getProgress() {
    return $this->_batch->getProgress();
  }

  /**
   * Run this runner. It will run until it reached either its time limit or
   * the end of the the batch.
   */
  public function run() {
    try {
      $this->timerStart();
      $this->sendEvent(self::EVENT_RUNNING);

      // Let's go for a ride.
      while (!$this->_batch->isFinished()) {

        // Fetch current operation and init it.
        $operation = $this->_batch->getOperationAt($this->_operationIndex);

        // Set operation limit only if we have one ourselves. Not having a limit
        // will force the operation to run its own default one.
        if ($this->_limit) {
          $operation->setLimit($this->_limit);
        }
        else {
          $operation->setLimit($operation->getDefaultLimit());
        }

        // Resume where asked.
        $operation->setOffset($this->_offset);

        // Run on the same operation by small lot until it finishes.
        while (!$operation->isFinished()) {
          $operation->process();

          $this->_offset = $operation->getOffset();
          $this->sendEvent(self::EVENT_ITERATION_DONE);

          // First timer check point, during iterations over the same operation.
          if ($this->timerElapsed()) {
            $this->sendEvent(self::EVENT_TIME_LIMIT_REACHED);
            break 2;
          }
        }

        // Reset offset for next iteration.
        $this->_offset = 0;

        // Advance to next operation. Batch finished test in the upper while will
        // avoid us to attempt fetching a non existing operation.
        ++$this->_operationIndex;

        $this->sendEvent(self::EVENT_OPERATION_CHANGE);

        // Second timer check point, before starting a new operation.
        if ($this->timerElapsed()) {
          $this->sendEvent(self::EVENT_TIME_LIMIT_REACHED);
          break;
        }
      }

      if ($this->_batch->isFinished()) {
        $this->sendEvent(self::EVENT_BATCH_FINISHED);
      }
      else if (isset($operation) && !$operation->isFinished) {
        // If we stopped, and operation is unfinished, keep its offset.
        $this->_offset = $operation->getOffset();
      }
    }
    catch (AdvancedBatch_Exception $e) {
      $this->sendEvent(self::EVENT_BATCH_ERROR);
    }
  }

  /**
   * Default constructor.
   * 
   * @param AdvancedBatch_Batch_Interface $batch
   *   The batch to run. If this batch is stalled, its state should already
   *   been restored internally.
   * @param int $operationIndex = 0
   *   Operation index at which this runner should start, useful when you are
   *   resuming a stalled batch.
   * @param int $offset = 0
   *   Offset to start at at the very first operation iteration, useful when
   *   you are resuming a stalled batch.
   * @param int $limit = 0
   *   Number of item to process per operation iteration. If you leave this
   *   to 0, for each operation the specific operation default limit will be
   *   used.
   * @param int $timeLimit = AdvancedBatch_Runner::TIME_LIMIT_DEFAULT
   *   Time limit in second this runner should proceed. When it reaches this
   *   time limit it stops further processing and accordingly send the event
   *   saying it reached it.
   */
  public function __construct(AdvancedBatch_Batch_Interface $batch, $operationIndex = 0, $offset = 0, $limit = 0, $timeLimit = AdvancedBatch_Runner::TIME_LIMIT_DEFAULT) {
    $this->_batch          = $batch;
    $this->_operationIndex = $operationIndex;
    $this->_offset         = $offset;
    $this->_limit          = $limit;
    $this->_timeLimit      = $timeLimit;
    // Resume the batch.
    $this->_batch->resume($this->_operationIndex, $this->_offset);
  }
}
