<?php

/**
 * Sample operation.
 */
class AdvancedBatch_Sample_Operation extends AdvancedBatch_Operation_Abstract {

  public function getDescription() {
    return t("Sample operation");
  }

  public function getDefaultLimit() {
    return 5;
  }

  /**
   * For sample purposes, number of iterations to do.
   * 
   * @var int
   */
  protected $_iterations;

  public function getTotal() {
    return $this->_iterations;
  }

  public function process() {
    $i = $this->_limit;
    while (0 < $i && ($this->_offset < $this->_iterations)) {

      sleep(1);

      ++$this->_offset;
      --$i;
    }
  }

  /**
   * Default constructor.
   */
  public function __construct($iterations) {
    $this->_iterations = $iterations;
  }
}
