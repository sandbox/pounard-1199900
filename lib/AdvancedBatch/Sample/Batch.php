<?php

/**
 * Sample batch.
 */
class AdvancedBatch_Sample_Batch extends AdvancedBatch_Batch_Abstract {

  public function getDescription() {
    return t("Sample batch");
  }

  protected function _initOperations() {
    $this->_operations = array();
    for ($i = 0; $i < $this->_options['operations']; ++$i) {
      $this->_operations[] = new AdvancedBatch_Sample_Operation($this->_options['iterations']);
    }
  }
}
