<?php

/**
 * Batch operation interface.
 * 
 * Operations do not carry any options, the owner batch should pass it when
 * it's being created or restored. These objects are volatile and are being
 * created on demand when the batch is created or restored.
 */
interface AdvancedBatch_Operation_Interface {
  /**
   * Get localized short description for admin UI.
   * 
   * @return string
   *   Can also return NULL.
   */
  public function getDescription();

  /**
   * Get operation weight.
   * 
   * Weight is not the operation order, but an arbitrary time consumption
   * measure, relative to other operations. This will help for generating
   * more accurate statistics during batch lifetime.
   * 
   * For example, a batch spawns 3 operations, one take one third of relative
   * time compared to the second in order to process one item in average, first
   * one would return 1 while second one would return 3.
   * 
   * A good practice is to use fibonacci series numbers for weighting, this
   * is not a precise exercise, it must only return an revelant order of
   * magnitude for the default progress computing to be at least a bit
   * accurate.
   * 
   * @return int
   */
  public function getWeight();

  /**
   * Get total number of items to process.
   * 
   * @return int
   */
  public function getTotal();

  /**
   * Get default item limit being processed at once when process() method is
   * called.
   * 
   * @return int
   */
  public function getDefaultLimit();

  /**
   * Caller may change the current item limit arbitrarily depending on the
   * execution context. When running in Drush we have no memory limit, so
   * the caller might raise the item limit in order to be more performant.
   * 
   * @param int $limit
   */
  public function setLimit($limit);

  /**
   * Get current offset.
   * 
   * @return int
   */
  public function getOffset();

  /**
   * Caller may force the current operation to change its offset arbitrarily.
   * This will be called in case of operation resuming after a batch stall.
   * 
   * @param int $offset
   */
  public function setOffset($offset);

  /**
   * Is this operation finished?
   * 
   * This will be called after each process() iteration and in other
   * integration points.
   * 
   * @return bool
   */
  public function isFinished();

  /**
   * Process items accordingly to currently set limit and offset. Internal item
   * limit will always be pragmatically set before run, even if the default
   * limit is used.
   * 
   * Once this method finished, new offset and limit should be set accordingly
   * so the caller can fetch the current values and do an accurate database
   * update. Any fail in providing the right figures here will cause
   * unpredictable behaviors when resuming operations.
   */
  public function process();
}
