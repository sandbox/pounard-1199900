<?php

/**
 * Base class for opertions.
 */
abstract class AdvancedBatch_Operation_Abstract implements AdvancedBatch_Operation_Interface {

  public function getWeight() {
    return 1;
  }

  public function getDefaultLimit() {
    return 20;
  }

  /**
   * @var int
   */
  protected $_limit = NULL;

  public function setLimit($limit) {
    $this->_limit = $limit;
  }

  /**
   * @var int
   */
  protected $_offset = 0;

  public function getOffset() {
    return $this->_offset;
  }

  public function setOffset($offset) {
    $this->_offset = $offset;
  }

  public function isFinished() {
    return $this->_offset >= $this->getTotal();
  }
}
