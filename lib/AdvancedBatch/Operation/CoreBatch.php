<?php

/**
 * procedural batch API wrapper.
 */
class AdvancedBatch_Operation_CoreBatch extends AdvancedBatch_Operation_Abstract {
  /**
   * @var args
   */
  protected $_args;

  /**
   * @var string
   */
  protected $_function;

  public function getDescription() {
    return t("Unknown core operation");
  }

  public function getTotal() {
    return 1; // FIXME: How to determine it?
  }

  public function isFinished() {
    // FIXME: Implement this.
    throw new AdvancedBatch_Exception("Not implemented yet.");
  }

  public function process() {
    // FIXME: Implement this.
    throw new AdvancedBatch_Exception("Not implemented yet.");
  }

  /**
   * Default constructor.
   * 
   * @param string $function
   * @param array $args = array()
   */
  public function __construct($function, array $args = array()) {
   $this->_function = $function;
   $this->_args = $args; 
  }
}
