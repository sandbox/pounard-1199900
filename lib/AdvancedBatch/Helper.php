<?php

/**
 * Static helper functions.
 */
class AdvancedBatch_Helper
{
  /**
   * Format with one (of year, month, day, hour, min, s) element granularity.
   */
  const FORMAT_TIME_MORE_THAN            = 1;

  /**
   * Format full human readable string.
   */
  const FORMAT_TIME_FULL                 = 2;

  /**
   * Format as fixed size string (HH:MM:SS) if possible.
   */
  const FORMAT_TIME_FIXED                = 3;

  /**
   * Estimate ETA.
   *
   * This algorithm is really simple, and might return Windows(c) minutes!
   * 
   * @param int $duration
   *   Time already taken, in seconds.
   * @param float $progress
   *   Progress indice.
   * 
   * @return int
   *   Estimated duration.
   */
  public static function estimateEta($duration, $progress) {
    if ($progress > 0) {
      // Another way of writing it:
      // return ((1 - $progress) * $duration) / $progress;
      // Fun note: doing some benchmarks, with 20 millions iterations
      // and incredebly stupid float values, both are more or less
      // exactly the same.
      // First one is from me, second one from Blaz.
      return $duration / $progress - $duration;
    }
  }

  /**
   * Format date in a custom short format.
   * 
   * @param int $date
   *   Unix timestamp.
   * 
   * @return string
   */
  public static function formatDate($date) {
    return format_date($date, 'custom', 'm/d H:i');
  }

  /**
   * Format progress value.
   * 
   * @param float $progress
   * 
   * @return string
   */
  public static function formatProgress($progress) {
    return sprintf("% 3d", floor($progress * 100)) . '%';
  }

  /**
   * Format duration.
   * 
   * @param int $duration
   *   Time count in seconds.
   * 
   * @return string
   */
  public static function formatDuration($duration, $format = self::FORMAT_TIME_FIXED) {
    if (empty($duration)) {
      return '0';
    }

    $output = '';

    // Fun hey?! I luuuuve this kind of code.
    $year  = floor( $duration / 31536000);
    $month = floor(($duration /  2592000) % 31536000);
    $day   = floor(($duration /    86400) %  2592000);
    $hour  = floor(($duration /     3600) %    86400);
    $min   = floor(($duration /       60) %     3600);
    $sec   = floor( $duration             %       60);

    switch ($format) {
      case self::FORMAT_TIME_FIXED:
      default:
        if ($duration < 86400) {
          $output .= sprintf("%02d", $hour) . ':';
          $output .= sprintf("%02d", $min ) . ':';
          $output .= sprintf("%02d", $sec );
          break;
        }
        // If more than one day, leave the "more than" formatting.

      case self::FORMAT_TIME_MORE_THAN:
        if ($year) {
          $output .= '> ' . format_plural($year,  "@count year",  "@count years");
        }
        else if ($month) {
          $output .= '> ' . format_plural($month, "@count month", "@count monthes");
        }
        else if ($day) {
          $output .= '> ' . format_plural($day,   "@count day",   "@count days");
        }
        else if ($hour) {
          $output .= '~ ' . format_plural($hour,  "@count h",     "@count h");
        }
        else if ($min) {
          $output .= '~ ' . format_plural($min,   "@count min",   "@count min");
        }
        else if ($sec) {
          $output .= '~ ' . format_plural($sec,   "@count s",     "@count s");
        }
        break;

      case self::FORMAT_TIME_FULL:
        if ($year) {
          $output .= format_plural($year,  "@count year",  "@count years")   . ' ';
        }
        if ($month) {
          $output .= format_plural($month, "@count month", "@count monthes") . ' ';
        }
        if ($day) {
          $output .= format_plural($day,   "@count day",   "@count days")    . ' ';
        }
        if ($hour) {
          $output .= format_plural($hour,  "@count h",     "@count h")       . ' ';
        }
        if ($min) {
          $output .= format_plural($min,   "@count min",   "@count min")     . ' ';
        }
        if ($sec) {
          $output .= format_plural($sec,   "@count s",     "@count s");
        }
        break;
    }

    return $output;
  }
}
