<?php

/**
 * The task object is an immutable view of a queued task at a certain point in
 * time. This object is readonly, which means that if you launch a runner, it
 * won't be updated accordingly.
 * 
 * We don't need it to be updated while running because we need the batches to
 * run in background, using a low level sync or async runner that bypasses PHP
 * memory and time limit, which will implement its own logic for watching the
 * running task.
 * 
 * The only task it is able to achieve is providing you the restored Batch
 * object from the database values and build a new runner instance for this
 * batch.
 * 
 * If you want to watch a running operation during runtime, please use a
 * specific implementation of AdvancedBatch_Listener_Interface that you must
 * register to the runner before running it.
 * 
 * All the accessors here will get data from the current database row, and
 * not from the actual batch instance.
 */
class AdvancedBatch_Task {
  /**
   * Task is running.
   */
  const STATE_RUNNING               = -10;

  /**
   * Task is being paused, waiting for iteration to finish.
   */
  const STATE_AWAITING_PAUSE        =  -2;

  /**
   * Task is being canceled, waiting for iteration to finish.
   */
  const STATE_AWAITING_CANCEL       =  -3;

  /**
   * Task is probably running, but runner is has reached its limit and is
   * waiting for the low level runner to resume it.
   */
  const STATE_STALLED               =  -4;

  /**
   * Task just been enqueued.
   */
  const STATE_QUEUED                =   0;

  /**
   * Task has been manually paused.
   */
  const STATE_PAUSED                =  21;

  /**
   * Task has been canceled.
   */
  const STATE_CANCELED              =  31;

  /**
   * Task has been canceled.
   */
  const STATE_ERROR                 =  41;

  /**
   * Task has finished.
   */
  const STATE_FINISHED              =  50;

  /**
   * Return the localized human readable status string.
   * 
   * @param int
   * 
   * @return string
   */
  public static function statusToString($status) {
    switch ($status) {

      case self::STATE_QUEUED:
        return t("Queued");

      case self::STATE_RUNNING:
        return t("Running");

      case self::STATE_PAUSED:
        return t("Paused");

      case self::STATE_AWAITING_PAUSE:
        return t("Pausing");

      case self::STATE_CANCELED:
        return t("Canceled");

      case self::STATE_AWAITING_CANCEL:
        return t("Canceling");

      case self::STATE_FINISHED:
        return t("Finished");

      case self::STATE_STALLED:
        return t("Stalled");

      case self::STATE_ERROR:
        return t("Error");
    }
  }

  /**
   * @var stdClass
   */
  protected $_row;

  /**
   * Get task identifier.
   * 
   * @return int
   *   NULL if task is not queued.
   */
  public function getTaskId() {
    return $this->_row->task_id;
  }

  /**
   * Set task identifier. Do not use unless your one contributor of this module
   * and you are messing with persistance.
   * 
   * @param int $taskId
   */
  public function _setTaskId($taskId) {
    $this->_row->task_id = $taskId;
  }

  /**
   * Get status.
   * 
   * @return int
   */
  public function getStatus() {
    return $this->_row->status;
  }

  /**
   * Get creation time.
   * 
   * @return int
   *   Unix timestamp.
   */
  public function getCreationTime() {
    return $this->_row->created;
  }

  /**
   * Get start time.
   * 
   * @return int
   *   Unix timestamp.
   */
  public function getStartTime() {
    return $this->_row->started;
  }

  /**
   * Get finished time.
   * 
   * @return int
   *   Unix timestamp.
   */
  public function getFinishedTime() {
    return $this->_row->finished;
  }

  /**
   * Get total duration, pauses excluded.
   * 
   * @return int
   *   Duration in seconds.
   */
  public function getDuration() {
    return $this->_row->duration;
  }

  /**
   * Get total duration, pauses excluded.
   * 
   * @return int
   *   Duration in seconds.
   */
  public function getClass() {
    return $this->_row->batch_class;
  }

  /**
   * Get saved progress.
   * 
   * @return float
   */
  public function getProgress() {
    // FIXME: This method shouldn't wake the object itself, it should rely
    // on a denormalized database saved progress value for performances
    // reasons.
    return $this->getBatch()->getProgress();
  }

  /**
   * Get backend.
   * 
   * FIXME: I'm not sure if this should live here.
   * 
   * @return AdvancedBatch_Backend_Interface
   *   NULL if none found.
   */
  public function getBackend() {
    if (isset($this->_row->backend)) {
      return AdvancedBatch_Backend::getBackend($this->_row->backend);
    }
    else {
      return NULL;
    }
  }

  /**
   * Get current operation index.
   * 
   * @return int
   */
  public function getOperationIndex() {
    return $this->_row->operation;
  }

  /**
   * Current instance. This may not be loaded, think about using the getBatch()
   * accessor instead, even for internal manipulation.
   * 
   * @var Advanced_Batch_Interface
   */
  protected $_batch;

  /**
   * Fully load associated batch using the database current information.
   * 
   * FIXME: This function should be protected, we should not allow fetching the
   * batch. Task is a purely informal class, existing for optimization.
   * 
   * @return AdvancedBatch_Batch_Interface
   */
  public function getBatch() {
    if (!isset($this->_batch)) {
      $this->_batch = new $this->_row->batch_class;
      $this->_batch->setOptions($this->_row->data);
      // FIXME: Forces the batch to initialize. We need to store progress into
      // the database row, will be better.
      $this->_batch->resume($this->_row->operation, $this->_row->offset);
    }
    return $this->_batch;
  }

  /**
   * Get task runner.
   * 
   * @param 
   * 
   * @return AdvancedBatch_Runner
   */
  public function getRunner($limit = 0, $timeLimit = AdvancedBatch_Runner::TIME_LIMIT_DEFAULT) {
    return new AdvancedBatch_Runner($this->getBatch(), $this->_row->operation, $this->_row->offset, $limit, $timeLimit);
  }

  /**
   * Default constructor.
   * 
   * @param string|object|Advanced_Batch_Interface $class
   *   If string is given, will use as class name.
   *   If an object is given, this must be the row from database, the
   *   associated batch can then be resumed.
   *   If a Advanced_Batch_Interface is given, this task will populate
   *   itself in order to be able to store and resume this batch state
   *   without the need of having a valid type.
   * @param array $options = NULL
   *   Specific implementation options, if any.
   */
  public function __construct($class, $options = NULL) {

    if ($class instanceof AdvancedBatch_Batch_Interface) {
      $this->_batch            = $class;
      $this->_row              = new stdClass;
      $this->_row->batch_class = get_class($class);
      $this->_row->task_id     = NULL;
      $this->_row->status      = self::STATE_QUEUED;
      $this->_row->created     = time();
      $this->_row->started     = 0;
      $this->_row->offset      = 0;
      $this->_row->finished    = 0;
      $this->_row->duration    = 0;
      $this->_row->operation   = 0;
      $this->_row->data        = $batch->getOptions();
    }
    else if (is_object($class)) {
      $this->_row = $class;
    }
    else if (is_string($class) && class_exists($class)) {
      $this->_row              = new stdClass;
      $this->_row->batch_class = $class;
      $this->_row->task_id     = NULL;
      $this->_row->status      = self::STATE_QUEUED;
      $this->_row->created     = time();
      $this->_row->started     = 0;
      $this->_row->offset      = 0;
      $this->_row->finished    = 0;
      $this->_row->duration    = 0;
      $this->_row->operation   = 0;
    }
    else {
      throw new AdvancedBatch_Exception("Invalid parameters at task creation.");
    }

    // Override options if provided.
    if (isset($options)) {
      $this->_row->data = $options;
    }
    else if (!isset($this->_row->data)) {
      $this->_row->data = array();
    }
  }
}
