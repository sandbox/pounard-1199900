<?php

/**
 * Defines a low level runner.
 */
interface AdvancedBatch_Backend_Interface {
  /**
   * Get maximum thread count.
   * 
   * @return int
   *   Non threaded runners should return 1 here.
   */
  public function getThreadMax();

  /**
   * Tell if current low level runner is already running.
   * 
   * @return int
   *   Number of running threads, O if not running.
   */
  public function isRunning();

  /**
   * Give to this runner the responsability of one existing enqueud, paused
   * or stalled task.
   * 
   * The real task assignment will be kept by the AdvancedBatch higher API,
   * this function is only here for the backend to be able to write it some
   * where it could need to. Some backend probably won't need to implement
   * this and will be able to read the queue table directly using the module
   * AdvancedBatch_Controller singleton for getting their tasks.
   * 
   * @param int $taskId
   */
  public function assignTask($taskId);

  /**
   * Get localized human name.
   * 
   * @return string
   */
  public function getName();

  /**
   * Get machine name.
   * 
   * @return string
   */
  public function getMachineName();

  /**
   * Check if can run. Whenever this instance returns FALSE, the backend will
   * be system-wide disabled until the site administrator manually re-enable
   * it. 
   * 
   * @return bool
   */
  public function isHealthy();
}
