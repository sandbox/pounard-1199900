<?php

/**
 * Drush specific listener.
 * 
 * This will be run in Drush context, so we are going to call Drush funtions
 * without any further checks.
 * 
 * Do not use outside Drush context, else PHP will WSOD.
 */
class AdvancedBatch_Drush_Listener extends AdvancedBatch_Task_Listener {

  protected function _batchError(AdvancedBatch_Runner $runner) {
    parent::_batchError($runner);
    drush_set_error(dt("Batch failed at @progress.", array('@progress' => AdvancedBatch_Helper::formatProgress($runner->getProgress()))));
  }

  protected function _batchFinished(AdvancedBatch_Runner $runner) {
    parent::_batchFinished($runner);
    drush_print(dt("Batch finished at @time.", array('@time' => format_date(time()))));
  }

  protected function _operationChange(AdvancedBatch_Runner $runner) {
    parent::_operationChange($runner);
    drush_print(dt('Switching to operation @operationIndex, @progress done', array(
      '@operationIndex' => $runner->getOperation() + 1,
      '@progress' => AdvancedBatch_Helper::formatProgress($runner->getProgress()),
    )));
  }

  protected function _timeLimitReached(AdvancedBatch_Runner $runner) {
    parent::_timeLimitReached($runner);
    drush_print(dt('Time limit reached, @progress done', array('@progress' => AdvancedBatch_Helper::formatProgress($runner->getProgress()))));
  }

  /**
   * Override default constructor to set backend.
   */
  public function __construct(AdvancedBatch_Task $task) {
    parent::__construct($task);
    $this->_backend = 'drush';
  }
}
