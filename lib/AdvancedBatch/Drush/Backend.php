<?php

/**
 * Defines a low level runner.
 */
class AdvancedBatch_Drush_Backend implements AdvancedBatch_Backend_Interface {

  public function getThreadMax() {
    return 1;
  }

  public function isRunning() {
    // FIXME: Undetermined?
    return FALSE;
  }

  /**
   * Nothing to do with this implementation, Drush will be able to get tasks
   * which are assigned to it directly using the module API.
   * 
   * @see AdvancedBatch_Backend_Interface::assignTask()
   */
  public function assignTask($taskId) {}

  public function getName() {
    return "Drush";
  }

  public function getMachineName() {
    return "drush";
  }

  public function isHealthy() {
    // FIXME: What!? How can I tell this?
    return TRUE;
  }
}
