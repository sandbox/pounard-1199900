<?php

/**
 * Advanced batch controller.
 * 
 * This class provide a set of helpers that will allow you to fetch and create
 * tasks n the task queue.
 * 
 * FIXME: Provide fine locking.
 */
class AdvancedBatch_Controller {
  /**
   * @var AdvancedBatch_Controller
   */
  private static $__instance;

  /**
   * Get instance.
   * 
   * @return AdvancedBatch_Controller
   */
  public static function getInstance() {
    if (!isset(self::$__instance)) {
      self::$__instance = new self;
    }
    return self::$__instance;
  }

  /**
   * Singleton pattern implementation.
   */
  private function __construct() {}

  /**
   * Create new task from given parameters.
   * 
   * @param string $class
   *   Class name.
   * @param array $options = NULL
   *   Specific implementation options, if any.
   */
  public function createTask($class, array $options = NULL) {
    return new AdvancedBatch_Task($class, $options);
  }

  /**
   * Load given task.
   * 
   * @param int|object $taskId
   *   Either the single task identifier, or the task row from database.
   * 
   * @return AdvancedBatch_Task
   */
  public function getTask($taskId) {
    if (is_object($taskId)) {
      $row = $taskId;
    }
    else {
      $row = db_select('advbatch_queue', 'a')
        ->fields('a')
        ->condition('task_id', $taskId)
        ->execute()
        ->fetchObject();

      if (!$row) {
        throw new AdvancedBatch_Exception("Task " . $taskId . " does not exists.");
      }

      $row->data = unserialize($row->data);
    }

    return new AdvancedBatch_Task($row);
  }

  /**
   * Queue new task.
   * 
   * @param AdvancedBatch_Task $task
   *   Task object.
   * @param int $mustRun = NULL
   *   Unix timestamp when the task should run.
   * @param int $discardAfter = NULL
   *   Unix timestamp when the task should be discarded if not started.
   */
  public function queueTask(AdvancedBatch_Task $task, $mustRun = NULL, $discardAfter = NULL) {
    $row = new stdClass;
    $row->batch_class   = $task->getClass();
    $row->status        = $task->getStatus();
    $row->operation     = 0;
    $row->offset        = 0;
    $row->must_run      = $mustRun;
    $row->discard_after = $discardAfter;
    $row->created       = time();
    $row->data          = serialize($task->getBatch()->getOptions());
    drupal_write_record('advbatch_queue', $row);
    // Update new object.
    $task->_setTaskId($row->task_id);
  }

  /**
   * Update task.
   * 
   * @param AdvancedBatch_Task $task
   *   Task to update.
   * @param array|object $values
   *   Values to changes.
   */
  public function updateTask(AdvancedBatch_Task $task, $values) {
    if (!is_array($values)) {
      $values = (array)$values;
    }
    $query = db_update('advbatch_queue')
      ->condition('task_id', $task->getTaskId())
      ->fields($values)
      ->execute();
  }

  /**
   * Remove a task from the queue.
   * 
   * @param int|AdvancedBatch_Task $task
   *   Task identifier or loaded object.
   */
  public function dequeueTask($task) {
    // FIXME: Implement this.
    throw new AdvancedBatch_Exception("Not implemented yet.");
  }

  /**
   * Get task queue.
   * 
   * @param array $conditions = array()
   *   Set of key/value pairs for filtering. Keys must be valid table row names
   *   while keys can be either associate values or values array for filtering.
   * @param int $offset = 0
   *   Start at.
   * @param int $limit = 0
   *   Stop at.
   * @param string $order = NULL
   *   String ordering field.
   * @param string $orderDesc = FALSE
   *   Set to TRUE for descending sort order.
   * 
   * @return array
   *   Array of AdvancedBatch_Task instances.
   */
  public function getTaskQueue(array $conditions = array(), $offset = 0, $limit = 24, $order = NULL, $ordeDesc = FALSE) {
    $ret = array();

    $query = db_select('advbatch_queue', 'a')
      ->fields('a');

    if (!empty($conditions)) {
      // FIXME: Implement this.
      throw new AdvancedBatch_Exception("Not implemented yet.");
    }

    if (isset($order)) {
      $query
        ->orderBy($order, $orderDesc ? 'DESC' : 'FALSE');
    }
    else {
      $query
        ->orderBy('status',  'ASC')
        ->orderBy('started', 'DESC')
        ->orderBy('created', 'DESC');
    }

    $rows = $query
      ->range($offset, $limit)
      ->execute()
      ->fetchAll();

    foreach ($rows as $row) {
      $row->data = unserialize($row->data);
      $ret[] = $this->getTask($row);
    }

    return $ret;
  }
}
