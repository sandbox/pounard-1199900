<?php

/**
 * Runner listener interface.
 */
interface Event_Listener_Interface {
  /**
   * Event happened.
   * 
   * @param Event_Sender_Interface $sender
   *   Sender.
   * @param mixed $message = NULL
   *   Message sent, type depends on the caller.
   * @param mixed $args = NULL
   *   Additional args the process could send.
   */
  public function event(Event_Sender_Interface $sender, $message = NULL, $args = NULL);
}
