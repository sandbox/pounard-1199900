<?php

/**
 * Event sender default implementation.
 */
class Event_Sender implements Event_Sender_Interface {
  /**
   * @var array
   */
  protected $_listeners = array();

  /**
   * @see Event_Sender_Interface::addListener()
   */
  public function addListener(Event_Listener_Interface $listener) {
    // spl_object_hash() call will avoid duplicates.
    $this->_listeners[spl_object_hash($listener)] = $listener;
  }

  /**
   * @see Event_Sender_Interface::removeListener()
   */
  public function removeListener(Event_Listener_Interface $listener) {
    // spl_object_hash() call will avoid duplicates.
    $key = spl_object_hash($listener);
    unset($this->_listeners[$key]);
  }

  /**
   * @see Event_Sender_Interface::sendEvent()
   */
  public function sendEvent($message = NULL, $args = NULL) {
    if (empty($this->_listeners)) {
      return;
    }
    foreach ($this->_listeners as $listener) {
      $listener->event($this, $message, $args);
    }
  }

  /**
   * @see Event_Sender_Interface::hasListeners()
   */
  public function hasListeners() {
    return !empty($this->_listeners);
  }
}
