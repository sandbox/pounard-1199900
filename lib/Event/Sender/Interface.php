<?php

/**
 * Runner listener interface.
 */
interface Event_Sender_Interface {
  /**
   * Add a listener.
   * 
   * @param Event_Listener_Interface $listener
   */
  public function addListener(Event_Listener_Interface $listener);

  /**
   * Remove a listener.
   * 
   * @param Event_Listener_Interface $listener
   */
  public function removeListener(Event_Listener_Interface $listener);

  /**
   * Raise an event call to listeners.
   * 
   * @param mixed $message = NULL
   *   Any message.
   */
  public function sendEvent($message = NULL);

  /**
   * Check if this observable has listeners.
   * 
   * @return boolean
   */
  public function hasListeners();
}
