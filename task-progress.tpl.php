<?php

/**
 * @file
 * Batch progress display.
 */
?>
<?php print $status . ' ' . $progress_string ?>
<div class="batch-progress" style="position: relative; border: 1px solid #ccc; height: 5px;">
  <div class="batch-progress-inside" style="background-color: #afa; position: absolute; top: 0; left: 0; bottom: 0; width: <?php print $progress * 100; ?>%;">
  </div>
</div>
