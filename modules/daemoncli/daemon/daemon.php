#!/usr/bin/env php
<?php

/**
 * @file
 * Daemon CLI bootstrap.
 * 
 * This file is the only one that shall remain procedural code. Once the
 * process watch main loop will be run, we only will respond to events.
 */

/**
 * Define this PHP script path.
 */
define('SCRIPT_PATH', dirname(__FILE__));

/**
 * These global variables will be used in all child processes, and might
 * influence some Drupal hook execution as well.
 */ 
global $stop, $debug, $daemon_logging, $daemon_pid, $daemonize;

/**
 * Global defaults.
 */
$debug                 = FALSE; // Global debug mode.
$stop                  = FALSE; // Helper for the sig_handler().
$daemon_logging        = FALSE; // Should the daemon do file logging?
$daemon_pid            = -1;    // Current process pid.
$daemonize             = TRUE;  // daemonize.

// Load required constants.
require_once SCRIPT_PATH . '/lib/defines.inc';

/**
 * Display a message. If logging is enabled, the message will be processed
 * throught logging, else if will be outputed in standard output.
 * 
 * @param string $message
 *   Message to display.
 */
function daemon_print($message) {
  global $debug, $daemonize;

  // If logging is enabled, let daemon output via logging.
  if (function_exists('daemoncli_logging_enabled') && daemoncli_logging_enabled()) {
    daemoncli_log_message($message);
  }
  // Never allow process linked to INIT (PID 1) output anything, it'd make
  // INIT very unhappy and could cause some weird behaviors with some old
  // kernels.
  else if (!$daemonize) {
    print $message . PHP_EOL;
  }
}

/**
 * Display help and exit.
 */
function daemon_usage() {
  // Minor fix, without this, daemon_print() won't allow console output.
  global $daemonize;
  $daemonize = FALSE;
  // Display helper.
  $cmd = $_SERVER['argv'][0];
  daemon_print("DaemonCLI " . DAEMONCLI_VERSION);
  daemon_print("Usage: " . $cmd . " [ARG [ARG .. ]]");
  daemon_print("Where ARG can be:");
  daemon_print("\t-d\t--debug\t\tEnable debug mode (extra verbose)");
  daemon_print("\t\t\t\tIf you enable logging, also verbose watchdog");
  daemon_print("\t-l\t--logging\tEnable file logging");
  daemon_print("\t-h\t--help\t\tDisplay this help and exit");
  exit(0);
}

/**
 * Parse command line and properly populate globals, using a simple getopt
 * emulation.
 */
function daemon_parse_command_line() {
  global $daemonize, $daemon_logging, $debug;

  $args = $_SERVER['argv'];

  // Skip first argument, which is command name.
  array_shift($args);

  // Check if arguments are present.
  if (0 === count($args)) {
    return;
  }

  foreach ($args as $key => $arg) {
    switch ($arg) {

      case "-d":
      case "--debug":
        daemon_print("Debug mode enabled, deamon will run in current console.");
        $debug = TRUE;
        $daemonize = FALSE;
        break;

      case '-l':
      case '--logging':
        daemon_print("Logging enabled.");
        $daemon_logging = TRUE;
        break;

      case '-h':
      case '--help':
        daemon_usage();

      default:
        daemon_print("Fatal error: Wrong command line switch '" . $arg . "'");
        exit(1);
    }
  }
}

/**
 * Custom autoloader.
 */
function _daemon_autoload($class_name) {
  static $path_search;

  if (!isset($path_search)) {
    $path_search = array(
      SCRIPT_PATH . '/lib',
      // FIXME: We need advbatch module for us to work.
      SCRIPT_PATH . '/../../../lib',
    );
  }

  foreach ($path_search as $path) {
    $file = $path . '/' . implode('/', explode('_', $class_name)) . '.php';

    if (file_exists($file)) {
      require_once $file;
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * We need a working autoloader.
 * 
 * FIXME: Later when Drupal will be bootstrapped, we need to switch this
 * to the end of the autoloader chain to ensure best performances.
 */
function daemon_autoload_init() {
  spl_autoload_register('_daemon_autoload');
}

/**
 * Check environment and die if it does not fit for running daemon.
 */
function daemon_check_env() {

  // Make sure there is not time limit, in most case, this is useless.
  set_time_limit(0);

  // Add some missing super globals in order to avoid some warnings output.
  if (!isset($_SERVER['REMOTE_ADDR'])) {
    $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
  }
  if (!isset($_SERVER['REQUEST_METHOD'])) {
    $_SERVER['REQUEST_METHOD'] = NULL;
  }
  if (!isset($_SERVER['SERVER_SOFTWARE'])) {
    $_SERVER['SERVER_SOFTWARE'] = 'DaemonCLI';
  }

  // Check for posix and pcntl_*() functions.
  if (!function_exists("pcntl_fork") || !function_exists('posix_getpid')) {
    throw new Exception("Could not find the PCNTL or POSIX extension.");
  }
}

/**
 * Initialize logging.
 */
function daemon_logging_init() {
  global $daemon_logging;

  if (!$daemon_logging) {
    return;
  }

  require_once SCRIPT_PATH . '/lib/logging.inc';
  daemoncli_logging_init();
}

/**
 * Shutdown logging.
 */
function daemon_logging_shutdown() {
  if (function_exists('daemoncli_logging_enabled') && daemoncli_logging_enabled()) {
    daemoncli_logging_shutdown();
  }
}

/**
 * Parent process.
 */
function daemon_parent_process_loop() {
  global $stop, $parent, $daemon_pid;

  // Intercept signals, only for parent process.
  declare(ticks = 1);

  // Attach our SIG handler to the parent process, not the console one.
  pcntl_signal(SIGTERM, "daemon_sig_handler");

  // Parent process is the session leader, this means that the PHP script that
  // has actually be run by the user in console will die. The parent loop
  // process will continue to run, but attached to INIT instead of the console.
  posix_setsid();

  /*
   * FIXME: We need to boostrap Drupal somehow to set this.
   * 
  variable_set(DAEMONCLI_PID, posix_getppid());
  variable_set(DAEMONCLI_START, time());
   */

  $parent = Daemon_Parent::getInstance();
  $handler = Daemon_Process::getInstance();
  $handler->addListener($parent);

  // Run watching loop and loose control of the current process. Now any
  // further business actions will be run by Daemon_Process instance events
  // triggered on child process spawn or die notification.
  // This is the most performant way of checking process because we don't
  // rely on any active polling loop by on lower layers system process
  // handling.
  $handler->start();
}

/**
 * SIG handler.
 * 
 * @param int $signo
 */
function daemon_sig_handler($signo) {
  global $stop;

  switch ($signo) {
    case SIGKILL:
    case SIGTERM:
      Daemon_Parent::getInstance()->shutdown();
      Daemon_Process::getInstance()->shutdown();

      // When reaching here, we are in the parent. Parent didn't bootstrapped
      // Drupal to avoid memory leaks and to ensure minimal performance impact
      // over time. We need the variable bootstrap Drupal for watchdog() and
      // variable_set() calls.
      Daemon_Drupal::bootstrap(Daemon_Drupal::BOOTSTRAP_VARIABLES);
      watchdog("daemoncli", "SIGTERM... Waiting end for process", array(), WATCHDOG_INFO);

      // Changing pid number to check if daemon is running.
      variable_set(DAEMONCLI_PID, 0);
      variable_set(DAEMONCLI_LAST_EXECUTE, 0);
      $stop = TRUE;
      break;
  }
}

/**
 * Run daemon parent process.
 */
function daemon_run_parent() {
  global $daemonize, $debug, $daemon_pid;

  // Do not run parent process if already running.
  if (-1 !== $daemon_pid) {
    daemon_print("Fatal error: Attempt to run a second parent process, exiting.");
    exit(1);
  }

  // No daemon mode, run the process in this console.
  if (!$daemonize || $debug) {
    daemon_parent_process_loop();
    return;
  }

  daemon_print("Starting daemon.");

  // Do an initial fork to run parent parent process in background.
  // Common daemonize technic.
  $pid = pcntl_fork();

  if (!$debug) {
    switch ($pid) {
      // Error, just die.
      case -1:
        throw new Exception("Could not fork the daemon parent process.");

      // Children.
      case 0:
        daemon_parent_process_loop();
        break;

      // Parent.
      default:
        $daemon_pid = $pid;
        daemon_print("Parent process run with pid " . $pid);

        // Forking specific, let the script exit gracefully while the real
        // parent process is now running on its own; detached from us.
        exit(0);
    }
  }
}

/**
 * Main function.
 */
function main() {
  try {
    // Proceed to daemon bootstrap.
    daemon_autoload_init();
    daemon_check_env();
    daemon_parse_command_line();
    daemon_logging_init();

    // Run the parent process.
    daemon_run_parent();

    // Children will never get here.
    daemon_logging_shutdown();

    // Exiting.
    daemon_print("Shutdown complete. Banzai!");
  }
  catch (Exception $e) {
    // FIXME: This last call may trigger errors.
    daemon_print("Fatal error: " . $e->getMessage());

    // Fatal error happened, cant proceed.
    exit(1);
  }

  // Normal exit.
  exit(0);
}

/**
 * Go for it baby!
 */
main();
