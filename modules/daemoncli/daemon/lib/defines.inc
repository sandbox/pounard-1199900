<?php

/**
 * @file
 * CLI Daemon constants. These are shared with the Drupal module.
 */

/**
 * Current version.
 */
define('DAEMONCLI_VERSION',              '7.x-1.x-dev build 20110707-1');

/**
 * Name of the 'pid' variable, stored when daemon is running in order to be
 * able to check for an existing running daemon or in order to kill it.
 */
define('DAEMONCLI_PID',                  'daemoncli_pid');

/**
 * Variable that tell if the daemon must run in debug mode or not when ran
 * via the Drupal administration pages.
 */
define('DAEMONCLI_UI_DEBUG_MODE',        'daemoncli_ui_debug_mode');

/**
 * Last execution variable name.
 */
define('DAEMONCLI_LAST_EXECUTE',         'daemoncli_last_execute');

/**
 * FIXME: Document.
 */
define('DAEMONCLI_START',                'daemoncli_start');

/**
 * FIXME: Document.
 */
define('DAEMONCLI_STOP',                 'daemoncli_stop');

/**
 * Does the Deamon has to run the cron variable name.
 */
define('DAEMONCLI_CRON',                 'daemoncli_cron');

/**
 * Daemon cron run delay variable name.
 */
define('DAEMONCLI_CRON_DELAY',           'daemoncli_cron_delay');

/**
 * Parent process child spawn delay default value.
 */
define('DAEMONCLI_DEFAULT_DELAY',        500000);

/**
 * Parent process child spawn delay default value.
 */
define('DAEMONCLI_DEFAULT_CHILD_LIMIT',  4);
