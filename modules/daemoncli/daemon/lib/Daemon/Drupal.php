<?php

/**
 * Bootstrap helper for bootstrapping Drupal.
 */
class Daemon_Drupal {
  /**
   * No boostrap.
   */
  const BOOTSTRAP_NONE            =   0;

  /**
   * Bootstrap database level.
   */
  const BOOTSTRAP_DATABASE        =  20;

  /**
   * Bootstrap level variables.
   */
  const BOOTSTRAP_VARIABLES       =  25;

  /**
   * Bootstrap full level, altered for no HTTP output.
   */
  const BOOSTRAP_ALTERED_FULL     =  30;

  /**
   * Actual bootstrap level.
   */
  protected static $_level = self::BOOTSTRAP_NONE;

  /**
   * Statically cached available bootstrap levels.
   * 
   * @var array
   */
  protected static $_levels;

  protected static $_modules;

  /**
   * Find Drupal root withing the local file directory parents.
   * 
   * @return string
   *   Drupal root path.
   * 
   * @throws Exception
   *   If path not found.
   */
  protected static function _findDrupalRoot() {
    $path = explode('/', substr(realpath(dirname(__FILE__)), 1));

    while ($dir = array_pop($path)) {
      $current = '/' . implode('/', $path);

      // FIXME: Is there a better way to footprint a Drupal instance?
      if (file_exists($current . '/index.php')) {
        return $current;
      }
    }
  }

  /**
   * Find module path.
   * 
   * @param string $module
   *   Module machine name.
   * 
   * @return string
   *   Module full path relative to system root.
   */
  public static function findModulePath($module) {
    self::init();

    // FIXME: Sorry, this is ugly, to lazy to write my own function here.
    // This should be done for memory and cpu consumption purposes.
    require_once DRUPAL_ROOT . '/includes/file.inc';
    $files = file_scan_directory(DRUPAL_ROOT, '/^' . $module . '.info$/');

    if (empty($files) || 1 !== count($files)) {
      throw new Exception("Unable to find module path for module " . $module . ".");
    }

    $file = array_shift($files);
    // Hopefully, modules are not using Drupal file API custom URI scheme.
    return dirname($file->uri);
  }

  /**
   * Init Drupal minimalist environment, this includes finding Drupal root
   * and setting up some global scoped constants.
   */
  public static function init() {
    if (defined('DRUPAL_ROOT')) {
      return;
    }

    if ($root = self::_findDrupalRoot()) {
      define('DRUPAL_ROOT', $root);
    }
    else {
      // This is a real fatal error, we cannot continue here.
      throw new Exception("Drupal root not found in parent directory tree.");
    }

    self::$_levels = array(
      self::BOOTSTRAP_DATABASE,
      self::BOOTSTRAP_VARIABLES,
      self::BOOSTRAP_ALTERED_FULL,
    );

    self::$_modules = array();
  }

  /**
   * Go for bootstrap.
   */
  protected static function _bootstrap($level) {
    switch ($level) {

      case self::BOOTSTRAP_DATABASE:
        // Fist, load whatever we need from Drupal.
        require_once DRUPAL_ROOT . '/includes/bootstrap.inc';

        // Replaces _drupal_bootstrap_configuration() in order to remove the useless
        // drupal_environment_initialize() call, because Drupal set headers at this
        // early bootstrap phase.
        set_error_handler('_drupal_error_handler');
        set_exception_handler('_drupal_exception_handler');
        drupal_settings_initialize();

        // Replaces _drupal_bootstrap_page_cache() because it triggers header
        // sending. Only initialize cache system.
        require_once DRUPAL_ROOT . '/includes/cache.inc';
        foreach (variable_get('cache_backends', array()) as $include) {
          require_once DRUPAL_ROOT . '/' . $include;
        }

        _drupal_bootstrap_database();
        break;

      case self::BOOTSTRAP_VARIABLES:
        _drupal_bootstrap_variables();
        break;

      case self::BOOSTRAP_ALTERED_FULL:
        require_once DRUPAL_ROOT . '/' . variable_get('session_inc', 'includes/session.inc');
        drupal_session_initialize();

        // Replaces _drupal_bootstrap_page_header(), because Drupal doesn't
        // catch up it is being run in CLI.
        bootstrap_invoke_all('boot');

        drupal_language_initialize();

        require_once DRUPAL_ROOT . '/includes/common.inc';
        _drupal_bootstrap_full();
        break;

      default:
        // Unknown bootstrap level, just return.
        throw new Exception("Unknown bootstrap level " . $level);
    }

    self::$_level = $level;
  }

  /**
   * Bootstrap Drupal or raise bootstrap level.
   */
  public static function bootstrap($level) {
    self::init();
    foreach (self::$_levels as $next) {
      if ($level < $next) {
        return;
      }
      self::_bootstrap($next);
    }
  }
}
