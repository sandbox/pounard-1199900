<?php

/**
 * Parent process specific methods.
 */
class Daemon_Parent implements Event_Listener_Interface {
  /**
   * @var Daemon_Parent
   */
  private static $__instance;
  
  /**
   * Get singleton instance.
   * 
   * @return Daemon_Parent
   */
  public static function getInstance() {
    if (!isset(self::$__instance)) {
      self::$__instance = new self;
    }
    return self::$__instance;
  }

  /**
   * Singleton pattern implementation.
   */
  private function __construct() {}

  /**
   * @var int
   */
  protected $_processLimit = DAEMONCLI_DEFAULT_CHILD_LIMIT;

  /**
   * Send the shutdown order to this instance.
   */
  public function shutdown() {
    // Do not let this instance spawn children anymore.
    $this->_processLimit = 0;
  }

  /**
   * Spawn missing children.
   */
  public function spawnChildren() {
    $handler = Daemon_Process::getInstance();
    // Spawn child processes while limit not reached.
    for ($i = count($handler); $i < $this->_processLimit; $i++) {
      $handler->runChild(new Daemon_Child);
    }
  }

  /**
   * Event listener. Implemented for responding to process handler main loop
   * events.
   * 
   * Once the main loop is started, we have no control on the parent process
   * anymore, responding to event allows us to ensure this process will sleep
   * most of time, while waiting for children processes to exit.
   * 
   * We don't have anything to do while the maximum number of processses is
   * reached except waiting for at least one the die in order to run a new one.
   * Using the handler event will help us doing that.
   * 
   * Remember that stuff which happens here must remain extremely fast to run,
   * because once we run children, the main loop must at all cost return in
   * child wait status: if it doesn't, it will miss one or more child exit and
   * those child will go to defunct state, the OS will wait for us  to fetch
   * back child status before letting it die.
   * 
   * @see Event_Listener_Interface::event()
   */
  public function event(Event_Sender_Interface $sender, $message = NULL, $args = NULL) {
    if (!$sender instanceof Daemon_Process) {
      return;
    }

    switch ($message) {

      case Daemon_Process::EVENT_LOOP_RUNNING:
      case Daemon_Process::EVENT_CHILD_EXITED:
        $this->spawnChildren();
        break;

      case Daemon_Process::EVENT_LOOP_EXITING:
        break;

      case Daemon_Process::EVENT_CHILD_SPAWNED:
        break;
    }
  }
}
