<?php

/**
 * Process handler singleton.
 */
class Daemon_Process extends Event_Sender implements Countable {
  /**
   * A child has been spanwed.
   */
  const EVENT_CHILD_SPAWNED              = 1;

  /**
   * A child exited.
   */
  const EVENT_CHILD_EXITED               = 2;

  /**
   * Main loop running.
   */
  const EVENT_LOOP_RUNNING               = 3;

  /**
   * Main loop exiting.
   */
  const EVENT_LOOP_EXITING               = 4;

  /**
   * @var Daemon_Process
   */
  private static $__instance;

  /**
   * Get singleton instance.
   * 
   * @return Daemon_Process
   */
  public static function getInstance() {
    if (!isset(self::$__instance)) {
      self::$__instance = new self;
    }
    return self::$__instance;
  }

  /**
   * Singleton pattern implementation.
   */
  private function __construct() {}

  /**
   * @var boolean
   */
  protected $_shutdownAsked = FALSE;

  /**
   * Send the shutdown order to this instance.
   */
  public function shutdown() {
    $this->_shutdownAsked = TRUE;
  }

  /**
   * Process registry.
   */
  protected $_children = array();

  /**
   * Run child process with the given callback.
   * 
   * @param Daemon_Process_Interface|callback $child
   *   Daemon_Process_Interface instance, or any callable (callable nature
   *   depends on PHP version).
   * 
   * @return int
   *   Child PID, or FALSE in case of error.
   */
  public function runChild($child) {
    global $daemon_pid; // FIXME: Shouldn't be done here?

    $pid = pcntl_fork();

    switch ($pid) {

      case -1:
        // Fork error, continue silentely.
        return FALSE;

      case 0:
        // Child process..
        // First of all, fix current process pid.
        $daemon_pid = posix_getpid();

        if ($child instanceof Daemon_Process_Interface) {
          $status = (int)$child->run();
        }
        else if (is_callable($child)) {
          $status = (int)call_user_func_array($callback, isset($args) ? $args : array());
        }
        exit($status);

      default:
        // Parent process.
        // Register new child process.
        $this->_children[$pid] = $child;

        // Build event arguments and send event.
        $args      = new stdClass;
        $args->pid = $pid;
        $this->sendEvent(self::EVENT_CHILD_SPAWNED, $args);

        return $pid;
    }
  }

  /**
   * Run the child process watch loop.
   * 
   * Important notice: once this is run, the only way you can interact with
   * the daemon will be using the event system.
   * 
   * @see Event_Listener_Interface
   * @see Event_Sender_Interface
   */
  public function start() {

    $this->sendEvent(self::EVENT_LOOP_RUNNING);

    while (TRUE) {

      // Exit if asked.
      if ($this->_shutdownAsked) {
        // We must wait for all children to die.
        if (empty($this->_children)) {
          break;
        }
      }

      // The WUNTRACED constant ensure the already dead children capture.
      // When listeners are processing signal, we are not in wait state anymore,
      // during this short amount of time any child can exit without us knowing
      // it.
      $pid = pcntl_wait($status, WUNTRACED);

      if (-1 === $pid) {
        // Getting here means an error happend, probably there is not children
        // running, this doesn't mean that we should exit.
        continue;
      }

      // Fetch the process exit status code. When doing this the process, which
      // is actually a zombie on the system, will be freed and die normally.
      $exitcode = pcntl_wexitstatus($status);

      // Unregister dead process.
      unset($this->_children[$pid]);

      // Build event arguments and send event.
      $args           = new stdClass;
      $args->pid      = $pid;
      $args->status   = $status;
      $args->exitcode = $exitcode;
      $this->sendEvent(self::EVENT_CHILD_EXITED, $args);
    }

    $this->sendEvent(self::EVENT_LOOP_EXITING);
  }

  /**
   * @see Countable::count()
   */
  public function count() {
    return count($this->_children);
  }
}
