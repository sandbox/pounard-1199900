<?php

/**
 * Daemon child process runner interface.
 */
interface Daemon_Process_Interface {
  /**
   * Method called by the parent process handler when this child process
   * is being run.
   * 
   * @return int
   *   Status code. If not set, 0 will be used instead for clean exit.
   */
  public function run();
}
