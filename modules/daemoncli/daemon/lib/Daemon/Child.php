<?php

/**
 * Daemon child. This particular class will be able to bootstrap full Drupal
 * and use it for common tasks.
 */
class Daemon_Child implements Daemon_Process_Interface {
  /**
   * @see Daemon_Process_Interface::run()
   */
  public function run() {
    global $debug;

    // Create drupal environment.
    Daemon_Drupal::bootstrap(Daemon_Drupal::BOOSTRAP_ALTERED_FULL);
    $this->_setDrupalEnv();

    $has_run = FALSE;

    // FIXME: Do the magic here.

    // Else, we have to do something!
    if (!$has_run && variable_get(DAEMONCLI_CRON, FALSE)) {
      $this->_cronRun();
    }

    /*
    // Check if admin asks to kill daemon
    if (variable_get(DAEMONCLI_STOP, 0) == 1) {
      if ($debug) {
        watchdog('daemoncli', "Exit by user interface", NULL, WATCHDOG_DEBUG);
      }

      // Get parent pid
      $pid = variable_get(DAEMONCLI_PID, 0);

      if ($pid == 0) {
        watchdog("daemoncli", 'Unable to find pid', NULL, WATCHDOG_ERROR);
        return;
      }

      watchdog("daemoncli", 'Daemon stopped by user interface', NULL, WATCHDOG_INFO);
      variable_set(DAEMONCLI_STOP, 0);

      // Sending signal to parent
      posix_kill($pid, SIGTERM);
    }
     */

    return;
  }

  /**
   * Run the Drupal cron.
   */
  protected function _cronRun() {
    global $debug;

    $last = variable_get('cron_last', 0);
    $delay = variable_get(DAEMONCLI_CRON_DELAY, 1);
    if ($delay < 1) {
      $delay = 10; 
    }

    if ($last + $delay * 60 < time()) {
      drupal_cron_run();
      if ($debug) {
        watchdog('daemoncli', "Ran cron", NULL, WATCHDOG_DEBUG);
      }
    }
  }

  /**
   * Set some Drupal environement contextual information, such as override the
   * current logged in user.
   */
  protected function _setDrupalEnv() {
    global $user, $debug;

    // Login as given user
    if (module_exists('daemoncli')) {
      $user = daemoncli_get_user();
    }
    else {
      // Failsafe option, daemon can run without its module.
      $user = user_load(1);
    }

    if ($debug) {
      watchdog('daemoncli', "Drupal child boostrap using '" . $user->name . "' user", NULL, WATCHDOG_DEBUG);
    }

    // FIXME: Session commit should be disabled?
  }
}
