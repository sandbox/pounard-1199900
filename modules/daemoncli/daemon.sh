#!/usr/bin/env sh
#
# Simple helper for running daemon.
# Easier to type than the full command line.
#

PHP="`which php`"
DAEMON_PATH="`dirname $0`/daemon/daemon.php"
$PHP -f $DAEMON_PATH -- "$@"
