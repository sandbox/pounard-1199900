<?php

/**
 * @file
 * CLI Daemon drush integration. 
 */

/**
 * Implementation of hook_drush_command().
 */
function daemoncli_drush_command() {
  $items = array();
  $items['daemon-run'] = array(
    'description' => 'Run daemon master process.',
    'aliases'     => array('dar'),
  );
  $items['daemon-kill'] = array(
    'description' => 'Kill and cleanup daemon state.',
    'aliases'     => array('dak'),
  );
  $items['daemon-status'] = array(
    'description' => 'Display daemon status.',
    'aliases'     => array('das'),
  );
  return $items;
}

function drush_daemoncli_daemon_run() {
  if (FALSE === module_exists('daemoncli')) {
    drush_set_error(dt("Deamon CLI module must be enabled on the Drupal instance."));
    return -1;
  }

  if (daemoncli_is_running()) {
    drush_print(dt("Daemon already running"));
    return;
  }

  $daemon_path = escapeshellcmd(drupal_get_path('module', 'daemoncli') . "/daemon.php");

  // Launch process.
  shell_exec("php -f " . $daemon_path . " > /dev/null");

  drush_log(dt("Starting daemon..."), 'success');

  // Ensure process has been run.
  // FIXME: Variables are statically cached, we cannot do it this way.
  /*
  if (daemoncli_is_running()) {
    drush_log(dt("Daemon is now running with pid @pid.", array('@pid' => variable_get(DAEMONCLI_PID, 0))), 'success');
  }
  else {
    drush_set_error(dt("Daemon could not be started."));
    return -1;
  }
   */
}

function drush_daemoncli_daemon_kill() {
  $daemon_pid = variable_get(DAEMONCLI_PID, 0);

  // Check if daemon is running
  if (!$daemon_pid) {
    drush_print(dt("No running daemon or pid file has been removed."));
    return;
  }

  // Check if daemon pid is in memory
  if (daemoncli_is_running() == FALSE) {
    drush_print(dt("Daemon is not running, changing memory state."));
    variable_set(DAEMONCLI_PID, 0);
    variable_set(DAEMONCLI_LAST_EXECUTE, 0);
    return;
  } 

  // Sending the SIGTERM signal to daemon
  drush_log(dt("Sending SIGTERM signal to daemon."), 'success');
  posix_kill($daemon_pid, SIGTERM);
}
